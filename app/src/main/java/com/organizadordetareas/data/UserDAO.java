package com.organizadordetareas.data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.organizadordetareas.model.User;

@Dao
public interface UserDAO {

    @Query("SELECT * FROM User WHERE username= :username and password= :password")
    User getUser(String username, String password);

    @Query("SELECT id, username,firstname,lastname,is_admin from User")
    User[] getAllUsers();

    @Query("SELECT COUNT(*) from User WHERE is_admin=1")
    int checkAdminExistence();

    @Insert
    void insert(User user);

    @Update
    void update(User user);

    @Delete
    void delete(User user);
}
