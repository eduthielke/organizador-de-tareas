package com.organizadordetareas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.organizadordetareas.data.UserDAO;
import com.organizadordetareas.data.UserDataBase;
import com.organizadordetareas.model.User;

public class RegisterActivity extends AppCompatActivity {

    EditText editTextUsername, editTextFirstname, editTextLastname, editTextPassword, editTextCnfPassword;
    Button buttonRegister;
    TextView textViewLogin;
    CheckBox checkBoxIsAdmin;
    private UserDAO userDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editTextUsername = findViewById(R.id.editTextUsername);
        editTextFirstname = findViewById(R.id.editTextFirstname);
        editTextLastname = findViewById(R.id.editTextLastname);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextCnfPassword = findViewById(R.id.editTextCnfPassword);
        checkBoxIsAdmin = findViewById(R.id.checkBoxIsAdmin);
        buttonRegister = findViewById(R.id.buttonRegister);

        textViewLogin = findViewById(R.id.textViewLogin);
        textViewLogin.setOnClickListener(v -> startActivity(new Intent(RegisterActivity.this, LoginActivity.class)));

        userDao = Room.databaseBuilder(this, UserDataBase.class, "mi-database.db").allowMainThreadQueries()
                .build().getUserDao();

        if(userDao.checkAdminExistence()==0){
            checkBoxIsAdmin.setVisibility(View.VISIBLE);
        } else {
            checkBoxIsAdmin.setVisibility(View.GONE);
        }


        buttonRegister.setOnClickListener(v -> {
            String username = editTextUsername.getText().toString().trim();
            String firstname = editTextFirstname.getText().toString().trim();
            String lastname = editTextLastname.getText().toString().trim();
            String password = editTextPassword.getText().toString().trim();
            String passwordConf = editTextCnfPassword.getText().toString().trim();
            Boolean is_admin = checkBoxIsAdmin.isChecked();
            if (password.equals(passwordConf)) {
                User user = new User(username,password,firstname,lastname,is_admin);
                userDao.insert(user);
                Intent moveToLogin = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(moveToLogin);

            } else {
                Toast.makeText(RegisterActivity.this, "Password is not matching", Toast.LENGTH_SHORT).show();
            }
        });

    }
}