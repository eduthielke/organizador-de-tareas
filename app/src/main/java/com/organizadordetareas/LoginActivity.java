package com.organizadordetareas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.organizadordetareas.data.UserDAO;
import com.organizadordetareas.data.UserDataBase;
import com.organizadordetareas.model.User;

public class LoginActivity extends AppCompatActivity {
    EditText editTextUsername, editTextPassword;
    Button buttonLogin;
    TextView textViewRegister;
    UserDAO db;
    UserDataBase dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonLogin = findViewById(R.id.buttonLogin);
        textViewRegister = findViewById(R.id.textViewRegister);

        dataBase = Room.databaseBuilder(this,UserDataBase.class,"mi-database.db")
                .allowMainThreadQueries()
                .build();
        db = dataBase.getUserDao();

        textViewRegister.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, RegisterActivity.class)));

        buttonLogin.setOnClickListener(v -> {
            String username = editTextUsername.getText().toString().trim();
            String password = editTextPassword.getText().toString().trim();

            User user = db.getUser(username, password);
            if (user != null) {
                Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                i.putExtra("User", user);
                startActivity(i);
                finish();
            }else{
                Toast.makeText(LoginActivity.this, "Unregistered user, or incorrect", Toast.LENGTH_SHORT).show();
            }
        });


    }
}